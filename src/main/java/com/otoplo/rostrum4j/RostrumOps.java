package com.otoplo.rostrum4j;

public enum RostrumOps {

    GET_HISTORY("blockchain.address.get_history"),
    LIST_UNSPENT("blockchain.address.listunspent"),
    GET_BALANCE("blockchain.address.get_balance"),
    GET_TIP("blockchain.headers.tip"),
    GET_TX("blockchain.transaction.get"),
    GET_UTXO_INFO("blockchain.utxo.get");

    private final String method;

    RostrumOps(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }
}
