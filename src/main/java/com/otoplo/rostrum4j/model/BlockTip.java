package com.otoplo.rostrum4j.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BlockTip {

    private long height;
    private String hex;
}
