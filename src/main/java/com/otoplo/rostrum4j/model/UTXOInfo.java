package com.otoplo.rostrum4j.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UTXOInfo {

    private BigDecimal amount;
    private long height;
    private String scipthash;
    private String status;

    @JsonProperty("tx_hash")
    private String txHash;

    @JsonProperty("tx_idem")
    private String txIdem;

    @JsonProperty("tx_pos")
    private int txPos;
}
