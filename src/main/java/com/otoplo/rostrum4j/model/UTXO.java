package com.otoplo.rostrum4j.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UTXO {

    private long height;

    @JsonProperty("outpoint_hash")
    private String outpointHash;

    @JsonProperty("tx_hash")
    private String txHash;

    @JsonProperty("tx_pos")
    private int txPos;

    private BigDecimal value;

}
